# RISC-V Code

### RISC-V Code In One Place


## Requirements
* _riscv64-elf_ toolchain to build FreeRTOS or Bare Metal
* _qemu-system-riscv32_ or _qemu-system-riscv64_ to run Virtual Machines

## Directory Structure

~~~console
$RISCV_CODE
├── freertos
│   ├── FreeRTOSConfig.h
│   ├── include
│   ├── src
│   └── license.txt
├── platform
│   └── xxx
│       ├── include
│       ├── src
│       └── xxx.ld
├── templates
│   └── xxx
│       ├── app.c
│       ├── FreeRTOSConfig.h
│       └── Makefile
└── README.md
~~~

## Building

1. Export **$RISCV_CODE** variable or add it to your profile file.
	* `export RISCV_CODE=path_to_riscv_code`
2. Copy the template.
	* `cp -r $RISCV_CODE/templates/virt hello_virt`
3. Go to the coppied folder and edit `app.c`.
	* add your files.
	* modify **$SRC** variable in `Makefile`.
4. Build.
	* `make`.
5. Run.
	* `qemu-system-riscv32 -nographic -M virt -bios none -kernel ./build/RTOSApp.axf`
