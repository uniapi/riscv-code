#include <FreeRTOS.h>
#include <task.h>

void vTaskFunction(void *pvParameters)
{
	char *pcTaskName = (char *)pvParameters;
	const TickType_t xDelay250ms = pdMS_TO_TICKS(250);

	for ( ;; ) {
		vSendString(pcTaskName);
		vTaskDelay(xDelay250ms);
	}
}

static const char *pcTextForTask1 = "Hello,";
static const char *pcTextForTask2 = "Virt!";

int xMainApplication()
{
	xTaskCreate(vTaskFunction, "Task 1", 1000, (void *)pcTextForTask1, 1, NULL);
	xTaskCreate(vTaskFunction, "Task 2", 1000, (void *)pcTextForTask2, 2, NULL);
	vTaskStartScheduler();
}
